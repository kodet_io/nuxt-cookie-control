export default {
    barTitle: 'Cookies',
    barDescription: 'Používáme naše vlastní soubory cookie a soubory cookie třetích stran, abychom vám mohli zobrazit tyto webové stránky a mohli lépe porozumět tomu, jak je používáte, s cílem zlepšit nabízené služby. Pokud budete pokračovat v prohlížení máme za to, že jste se soubory cookie souhlasili.',
    acceptAll: "Přijmout vše",
    declineAll: "Odmítnout všechny",
    manageCookies: "Spravovat soubory cookie",
    unsaved: "Máte neuložená nastavení",
    close: "Zavřít",
    save: 'Uložit',
    necessary: "Nezbytné soubory cookie",
    optional: "Nepovinné soubory cookie",
    functional: "Funkční cookies",
    blockedIframe: 'Chcete-li zobrazit tuto stránku, povolte, prosím, funkční soubory cookie',
    here: 'zde',
    acceptNecessary: "Přijmout nezbytné"
  }
